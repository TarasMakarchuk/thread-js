const ButtonType = {
  BUTTON: 'button',
  SUBMIT: 'submit',
  SAVE: 'save',
  CANCEL: 'cancel'
};

export { ButtonType };
