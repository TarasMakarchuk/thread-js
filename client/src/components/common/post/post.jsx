import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label, Button, Form } from 'src/components/common/common';
import { useSelector } from 'react-redux';

import styles from './styles.module.scss';

const Post = ({ post, onPostLike, onPostDislike, onExpandedPostToggle, sharePost, uploadImage, onPostUpdate }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;

  const { postOwner } = useSelector(state => ({
    postOwner: state.profile.user
  }));
  const [isTextAreaShown, setIsShowTextArea] = React.useState(false);
  const [updatedPostText, setUpdatedPostText] = React.useState(body);

  const [newImage, setImage] = React.useState(undefined);
  const [isUploading, setIsUploading] = React.useState(false);

  const isPostEditable = postOwner.id === post.user.id;
  const date = getFromNowTime(createdAt);

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);

  const handlePostEditable = () => {
    setIsShowTextArea(!isTextAreaShown);
  };

  const onPostTextChange = event => {
    setUpdatedPostText(event.target.value);
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
      })
      .catch(error => {
        console.log(error);
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  const onSavePostHandler = async () => {
    await onPostUpdate({
      postId: id,
      body: updatedPostText,
      imageId: newImage?.imageId
    });
    setIsShowTextArea(!isTextAreaShown);
  };

  const onCancelPostHandler = () => {
    setIsShowTextArea(!isTextAreaShown);
  };

  const handleExpandedPostToggle = () => onExpandedPostToggle(id);

  const updatePostBlock = () => (
    <>
      <Label as="a" color={ButtonColor.BLUE} ribbon className={styles.labelEditPost}>
        Edit post
      </Label>
      {newImage?.imageLink && (
        <div>
          <Image src={newImage?.imageLink} alt="post" />
        </div>
      )}
      <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button
          color="teal"
          isLoading={isUploading}
          isDisabled={isUploading}
          iconName={IconName.IMAGE}
          className={styles.btnChangeImage}
        >
          <label style={{ marginLeft: 10 }}>
            Change image
            <input
              name="image"
              type="file"
              onChange={handleUploadFile}
              hidden
            />
          </label>
        </Button>
      </div>
      <Form>
        <Form.TextArea
          name="body"
          value={updatedPostText}
          onChange={onPostTextChange}
          style={{ marginTop: 10 }}
        />
        <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
          <Button
            color={ButtonColor.TEAL}
            type={ButtonType.SAVE}
            onClick={onSavePostHandler}
            className={styles.btnSave}
          >
            Save
          </Button>
          <Button
            color={ButtonColor.BLUE}
            type={ButtonType.CANCEL}
            onClick={onCancelPostHandler}
            className={styles.btnCancel}
          >
            Cancel
          </Button>
        </div>
      </Form>
    </>
  );

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        {isTextAreaShown ? updatePostBlock() : <Card.Description>{body}</Card.Description>}
      </Card.Content>
      <Card.Content extra>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostLike}
        >
          <Icon name={IconName.THUMBS_UP} />
          {likeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostDislike}
        >
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => handlePostEditable(id)}
          style={{ float: 'right' }}
        >
          {!isTextAreaShown && isPostEditable && <Icon name={IconName.EDIT} />}
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onPostUpdate: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default Post;
