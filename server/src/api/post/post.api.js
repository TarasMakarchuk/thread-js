import { PostsApiPath } from '../../common/enums/enums';

const initPost = (Router, services) => {
  const { post: postService } = services;
  const router = Router();

  router
    .get(PostsApiPath.ROOT, (req, res, next) => postService
      .getPosts(req.query)
      .then(posts => res.send(posts))
      .catch(next))
    .get(PostsApiPath.$ID, (req, res, next) => postService
      .getPostById(req.params.id)
      .then(post => res.send(post))
      .catch(next))
    .post(PostsApiPath.ROOT, (req, res, next) => postService
      .create(req.user.id, req.body)
      .then(post => {
        req.io.emit('new_post', post); // notify all users that a new post was created
        return res.send(post);
      }).catch(next))
    .post(PostsApiPath.POST_REACTION, (req, res, next) => postService
      .getPostWithUserReaction(req.body.userId, req.body.postId)
      .then(post => {
        return res.json(post);
      })
      .catch(next))
    .put(PostsApiPath.ROOT, (req, res, next) => postService
      .updateById(req.body.postId, req.body)
      .then(post => {
        return res.json(post);
      })
      .catch(next))
    .put(PostsApiPath.REACT, (req, res, next) => postService
      .setReaction(req.user.id, req.body)
      .then(reaction => {
        if (reaction.post && reaction.dataValues.isLike === true && reaction.post.userId !== req.user.id) {
          // notify a user if someone (not himself) liked his post
          req.io
            .to(reaction.post.userId)
            .emit('like', 'Your post was liked!');
        }
        if (reaction.post && reaction.dataValues.isLike === false && reaction.post.userId !== req.user.id) {
          // notify a user if someone (not himself) disliked his post
          req.io
            .to(reaction.post.userId)
            .emit('dislike', 'Your post was disliked!');
        }
        return res.send(reaction);
      })
      .catch(next));

  return router;
};

export { initPost };
