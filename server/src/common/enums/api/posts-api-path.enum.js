const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACT: '/react',
  POST_REACTION: '/post-reaction',
};

export { PostsApiPath };
